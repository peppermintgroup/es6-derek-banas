// chapter 1: let

document.write("chapter 1: let <br>");

if (true) {
  var a = 1;
  document.write("a = " + a + "<br>");
}

document.write("a = " + a + "<br>");

var b = 10;
if (true) {
  let b = 20;
}

document.write("b = " + b + "<br>");

// chapter 2: const

document.write("<br> chapter 2: const <br>");

const PI = 3.14159;


if (true) {
  const PI = 2.14159;
  document.write("PI = " + PI + "<br>");
}

document.write("PI = " + PI + "<br>");

// chapter 3: datatypes

document.write("<br> chapter 3: datatypes <br>");

document.write(typeof true + " <br>");
document.write(typeof 3.14 + " <br>");
document.write(typeof "string" + " <br>");
document.write(typeof Symbol() + " <br>");
document.write(typeof {a:1} + " <br>");
document.write(typeof [1,2,3] + " <br>");
document.write(typeof undefined + " <br>");

// chapter 4: template literals

document.write("<br> chapter 4: template literals <br>");

let fName = "Derek";
let lName = "Banas";

document.write(`My first name is ${fName} and last name is ${lName} <br>`);

let num1 = 10;
let num2 = 5;

document.write(`10 * 5 = ${num1 * num2} <br>`);

// chapter 5: flexible expression interpolation

document.write(`<br> chapter 5: flexible expression interpolation<br>`);

function doMath(strings, ...values) {
    if(strings[0] == 'Add') {
        document.write(`${values[0]} + ${values[1]} = ${values[0] + values[1]}<br>`);
    } else if(strings[0] == 'Sub') {
        document.write(`${values[0]} - ${values[1]} = ${values[0] - values[1]}<br>`);
    }
}

doMath `Add${10} ${20}`;
doMath `Sub${10} ${20}`;


// chapter 6: string iteration

document.write(`<br> chapter 6: string iteration. <br>`);

for(let c of fName) {
    document.write(`${c} <br>`);
}

// chapter 7: string methods

document.write(`<br> chapter 7: string methods <br>`);


document.write("Hello ".repeat(3) + "<br>");

document.write(fName.startsWith("De") + "<br>");

document.write(fName.endsWith("ek") + "<br>");

document.write(fName.includes("ere") + "<br>");


// chapter 8: multi strings

document.write(`<br> chapter 8: multi strings <br>`);


let multilineStr = ` This is
a multiline
string`;

document.write(`${multilineStr} <br>`);

// chapter 9: default parameters of function

document.write(`<br> chapter 9: default parameters of function <br>`);

function getSum(num1 = 1, num2 = 1) {
    document.write(`${num1} + ${num2} = ${num1 + num2} <br>`);
    document.write(`${arguments[0]} + ${arguments[1]}<br>`);
}

getSum(3);


function getSumMore(...vals) {
    let sum = 0;
    for (let item of vals) {
        sum += item;
    }
    return sum;
}

let vals = [1,2,3,4,5];

document.write(`${getSumMore(...vals)} <br>`);

// chapter 10: arrow function

document.write(`<br> chapter 10: arrow function <br>`);

let difference = (num1, num2) => num1 - num2;

document.write(`5 - 10 = ${difference(5, 10)} <br>`);

let mult = (num1, num2) => {
    let product = num1 * num2;
    document.write(`${num1} * ${num2} = ${product}<br>`);
}

mult(5, 6);

let valArr = [1,2,3,4,5];

let sumVals = valArr.reduce((a, b) => a + b);
document.write(`Sum: ${sumVals} <br>`);

let evens = valArr.filter( v => v % 2 == 0 );
document.write(`Evens: ${evens} <br>`);

let doubles = valArr.map( v => v * 2);
document.write(`Doubles: ${doubles} <br>`);

// chapter 11: object literal

document.write(`<br> chapter 11: object literal <br>`);

function createAnimal(name, owner) {
    return {
        name,
        owner,
        getInfo() {
            return `${this.name} is owned by ${this.owner}`;
        },
        address: {
            street: '123 Main St',
            city: 'Pittsburg'
        }
    };
}

var spot = createAnimal('Spot', 'Doug');

document.write(`${spot.getInfo()}<br>`);

document.write(`${spot.name} is at ${spot.address.street}<br>`);

document.write(`${Object.getOwnPropertyNames(spot).join(" ")}<br>`);

// chapter 12: destructuring

document.write(`<br> chapter 12: destructuring <br>`);

let {name, owner} = spot;
document.write(`Name: ${name} <br>`);

let {address} = spot;
document.write(`Address: ${address.street} <br>`);

let favNums = [2.718, .5772, 4.6692];

let[,,chaos] = favNums;
document.write(`Chaos: ${chaos} <br>`);

let[,...last2] = favNums;
document.write(`2nd Num: ${last2[0]} <br>`);

let val1 = 1, val2 = 2;
[val1, val2] = [val2, val1];
document.write(`Val2: ${val2} <br>`);

// chapter 13: classes

document.write(`<br> chapter 13: classes <br>`);

class Mammal {
    constructor(name) {
        this._name = name;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    static makeMammal(name) {
        return new Mammal(name);
    }

    getInfo() {
        return `${this.name} is a mammal`;
    }
}

let monkey = new Mammal("Fred");

monkey.name = "Mark";
document.write(`Mammal : ${monkey.name} <br>`);

let chipmunk = Mammal.makeMammal("Chipper");
document.write(`Mammal 2 : ${chipmunk.name} <br>`);

class Marsupial extends Mammal {
    constructor (name, hasPouch) {
        super(name);
        this._hasPouch = hasPouch;
    }

    get hasPouch() {
        return this._hasPouch;
    }

    set hasPouch(hasPouch) {
        this._hasPouch = hasPouch;
    }

    getInfo() {
        return `${this.name} is a marsupial`;
    }
}

let kangaroo = new Marsupial("Paul", true);
document.write(`It is ${kangaroo.hasPouch} that ${kangaroo.name} has a pouch<br>`);
document.write(`${chipmunk.getInfo()} <br>`);
document.write(`${kangaroo.getInfo()} <br>`);

function getClass(classType) {
    if(classType == 1) {
        return Mammal;
    } else {
        return Marsupial;
    }
}

class Koala extends getClass(2) {
    constructor(name) {
        super(name);
    }
}

let carl = new Koala("Carl");
document.write(`${carl.getInfo()} <br>`);

// chapter 14: symbols

document.write(`<br> chapter 14: symbols <br>`);


let capital = Symbol("State capital");

let pennsylvania = {};
pennsylvania[capital] = "Harrisburg";
document.write(`Capital of PA : ${pennsylvania[capital]}<br>`);

document.write(`Symbol Capital: ${capital.toString()}<br>`);

let employNum = Symbol.for("Employee Number");

let bobSmith = {};
bobSmith[employNum] = 10;

let sallyMarks = {};
sallyMarks[employNum] = 11;

document.write(`Bob : ${bobSmith[employNum]}<br>`);
document.write(`Bob : ${sallyMarks[employNum]}<br>`);

// chapter 15: arrays

document.write(`<br> chapter 15: arrays <br>`);

let array1 = Array.of(1,2,3);

let array2 = Array.from("word");

let array3 = Array.from(array1, (value) => value * 2 );

for (let item of array3) document.write(`Array Val : ${item}<br>`);

// chapter 16: sets

document.write(`<br> chapter 16: sets <br>`);

let randSet = new Set();
randSet.add(10);
randSet.add("Word");

document.write(`Has 10 : ${randSet.has(10)}<br>`);
document.write(`Set size : ${randSet.size}<br>`);

randSet.delete(10);

for (let item of randSet) document.write(`Set val : ${item}<br>`);

// chapter 17: maps

document.write(`<br> chapter 17: maps <br>`);

let randMap = new Map();

randMap.set("key1", "Random string");
randMap.set("key2", 10);

document.write(`key1 : ${randMap.get("key1")}<br>`);
document.write(`key2 : ${randMap.get("key2")}<br>`);

document.write(`Size of map : ${randMap.size}<br>`);

randMap.forEach((value, key) => document.write(`${key} ${value}<br>`));

// chapter 18: promises

document.write(`<br> chapter 18: promises <br>`);

let p1 = Promise.resolve("Resolve me");
p1.then((res) => document.write(`${res} <br>`))

let p2 = new Promise((resolve, reject) => {
    // setTimeout(()=> resolve("Resolve me 2"), 2000);
});

p2.then((res) => document.write(`${res}<br>`));

let randVal = 18;

let p3 = new Promise((resolve, reject) => {
    if(randVal == 6){
        resolve("Good value");
    } else {
        reject("Bas value");
    }
});

p3.then(
    (val) => document.write(`${val}<br>`),
    (err) => document.write(`${err}<br>`)
);

let randVal2 = 16;

let p4 = new Promise((resolve, reject) => {
    if(randVal2 <= 17){
        throw new Error("Cant't Vote");
    } else {
        resolve("Can Vote");
    }
});


p4.then(
    (val) => document.write(`${val}<br>`)
).catch(
    (err) => document.write(`${err.message}<br>`)
);
